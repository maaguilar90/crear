/**
 * Created by danielzarate on 23/10/17.
 */


$(document).ready(function () {

    $('#btn_codigo').click(function () {
        event.preventDefault();
        console.log('consultacuponcodigo');
        consultaCupon("consultacuponcodigo");
    });

    $('#btn_codigo_tk').click(function () {
        event.preventDefault();
        console.log('consultacuponcodigo');
        consultaCupon("consultacuponcodigotk");
    });

    $('#btn_cedula').click(function () {
        event.preventDefault();
        var param = $('#cedula').val();
        var token = $('#token').val();

    });
});

function consultaCupon(action) {
    console.log('test codigo');
    var param = $('#codigo').val();
    var token = $('#token').val();
    $.post(action, {
        param: param,
        _csrf: token
    }).done(function (result) {
        data = JSON.parse(result);
        console.log('cupon');
        if (data.status == true) {
            var integracion = data.base_integracion;
            if (data.status_integracion) {
                var result = '<table>' +
                    '<tr><th>coupon_number</th><th>creation_date</th><th>local_code_emission</th><th>status</th><th>terminal_code</th><th>ticket</th></tr>' +
                    '<tr><td>' + integracion.coupon_number + '</td><td>' + integracion.creation_date + '</td><td>' + integracion.local_code_emission + '</td><td>' + integracion.status + '</td><td>' + integracion.terminal_code + '</td><td>' + integracion.ticket + '</td></tr>' +
                    '</table>';
                $('#res_integra').html(result);
            } else {
                $('#res_integra').html('<div><span>No existe el codigo</span></div>');
            }
            var landing = data.base_landing;
            if (data.status_landing) {
                var result = '<table>' +
                    '<tr><th>identity</th><th>lastname</th><th>name</th><th>address</th><th>birthdate</th><th>cellphone</th><th>city</th><th>codigo</th><th>creation_date</th><th>description</th><th>email</th><th>gender</th><th>num_tarjetamas</th><th>phone</th><th>provincia</th><th>sucursal</th><th>tarje</th><th>termi</th></tr>' +
                    '<tr><td>' + landing.identity + '</td><td>' + landing.lastname + '</td><td>' + landing.name + '</td><td>' + landing.address + '</td><td>' + landing.birthdate + '</td><td>' + landing.cellphone + '</td><td>' + landing.city + '</td><td>' + landing.codigo + '</td><td>' + landing.creation_date + '</td><td>' + landing.description + '</td><td>' + landing.email + '</td><td>' + landing.gender + '</td><td>' + landing.num_tajeta_mas + '</td><td>' + landing.phone + '</td><td>' + landing.province + '</td><td>' + landing.sucursal + '</td><td>' + landing.tarjetamas + '</td><td>' + landing.terminos + '</td></tr>' +
                    '</table>';
                $('#res_landing').html(result);
            } else {
                $('#res_landing').html('<div class="text-center"><span>No existe el codigo</span></div>');
            }
        } else {
            $('#res_landing').html('<div class="text-center"><span>No existe el codigo</span></div>');
            $('#res_integra').html('<div class="text-center"><span>No existe el codigo</span></div>');
        }
        //loading(0);
    });
}

function menu(page) {
    if (page == 1) {
        $('#page_premios').hide();
        $('#page_gye').show();
        $('#page1, #page2').removeClass('active');
        $('#page1').addClass('active');
    }
    if (page == 2) {
        $('#page_premios').show();
        $('#page_gye').hide();
        $('#page1, #page2').removeClass('active');
        $('#page2').addClass('active');
    }
}
