<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'Unidad Educativa Crear';
use yii\web\View;
use app\models\Slider;
use app\models\Popup;
$script=<<< JS
var global_brand='';
$(document).ready(function() {
	
});
JS;
$aux='';
$this->registerJs($script,View::POS_END);
?>
<?php
@$info=$_GET['info'];
if (@$info==1){ $infocontent='Gracias por registrarte en el sistema. Se ha enviado un correo electrónico para que puedas acceder a nuestros servicios. '; }
if (@$info==2){ $infocontent='El usuario ingresado ya se encuentra registrado en el sistema.'; }
if (@$info==3){ $infocontent='La contraseña se ha actualizado con éxito. Ahora puede iniciar sesión y acceder a nuestros servicios.'; }
if (@$info==4){ $infocontent='El token generado es inválido. En caso de tener inconvenientes contáctese con nosotros.'; }
?>


    
<section class="container-fluid">
	<div class="fotorama" data-autoplay="true">
        <?php $sliders = Slider::find()->OrderBy('orden')->all(); ?>
        <?php foreach ($sliders as $slider): ?>

		<div data-img="<?= URL::base() ?>/images/banner/<?= $slider->image ?>"><a href="<?= $slider->description ?>"></a></div>
        <?php endforeach ?>
	</div>

</section>
        <!-- -->


<section class="container-fluid">
    <div class="servicio">
        <div style="text-align: center;">
            <span>EDUCACIÓN</span>
            <div>
                <span class="line-center" style="font-size: 10px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            </div>
        </div>
    </div>
</section>

<section class="container-fluid">
    <div class="servicios-contenido" style="padding: 12%; padding-top: 5%; text-align: center; padding-bottom: 5%;">
      <div style="text-align: center;" class="servicios-individual">
           <div>
               <img src="<?= URL::base() ?>/images/site/maternal.fw.png">
               <br><br>
               <span>Maternal</span><br>
               <span class="line-center-blue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
               <br>
               <div class="servicio-content-text" style="text-align:center;">
                   <span >Educación Maternal</span>
                   <br><br>
                   <div style="text-align: center;">
                    <button class="vermas"  onclick="javascript:window.location.href='<?= URL::base() ?>/'">Ver más</button>
                   </div>
               </div>
           </div>
        </div>
        <div style="text-align: center;" class="servicios-individual">
           <div>
               <img src="<?= URL::base() ?>/images/site/cesantia.fw.png">
               <br><br>
               <span>Educación Inicial</span><br>
               <span class="line-center-blue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
               <br>
               <div class="servicio-content-text" style="text-align:center;">
                   <span >Educación Inicial</span>
                   <br><br>
                   <div style="text-align: center;">
                    <button class="vermas"  onclick="javascript:window.location.href='<?= URL::base() ?>/'">Ver más</button>
                   </div>
               </div>
           </div>
        </div>
        <div style="text-align: center;" class="servicios-individual">
           <div>
               <img src="<?= URL::base() ?>/images/site/prestamos.fw.png">
               <br><br>
               <span>Ciclo Básico</span><br>
               <span class="line-center-blue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
               <br>
               <div class="servicio-content-text" style="text-align:center;">
                   <span >Educación Ciclo Básico</span>
                   <br><br>
                   <div style="text-align: center;">
                    <button class="vermas"  onclick="javascript:window.location.href='<?= URL::base() ?>/'">Ver más</button>
                   </div>
               </div>
           </div>
        </div>
        <div style="text-align: center;" class="servicios-individual">
           <div>
               <img src="<?= URL::base() ?>/images/site/descargas.fw.png">
               <br><br>
               <span>Bachillerato</span><br>
               <span class="line-center-blue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
               <br>
               <div class="servicio-content-text" style="text-align:center;">
                   <span >Educación Bachillerato</span>
                   <br><br>
                   <div style="text-align: center;">
                    <button class="vermas" onclick="javascript:window.location.href='<?= URL::base() ?>/'">Ver más</button>
                   </div>
               </div>
           </div>
        </div>
    </div>
</section>

<section class="container-fluid">
    <div class="educacionfinanciera">
        <div class="column1">
            <img src="<?= URL::base() ?>/images/site/educacionfinanciera.fw.png">
        </div>
        <div class="column2">
            <div style=""><span class="titulo-plan">NUESTRA</span></div>
            <div style="">
                <br>
                <span class="titulo-ef">OFERTA EDUCATIVA</span>
                <br>
                <span class="line-center-blue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <br>
                <span class="title-content">Introducción</span>
                <br>
                <span class="text-content">La Unidad Educativa Crear, con una trayectoria de muchos años de reconocida experiencia en la formación de la niñez y juventud ecuatoriana, ofrece a la comunidad de Guayaquil, Samborondón y Daule un programa académico integral, con altos estándares nacionales e internacionales, desde Maternal hasta Bachillerato.</span>
                <br><br>
                <span class="title-content">Objetivos</span>
                <br>
                <span class="text-content">Para la educación de los niños, niñas y jóvenes del nuevo milenio, nuestra promesa de valor se sostiene en los siguientes pilares.</span>
                <br>
                <br>
                <a href="<?= URL::base() ?>/site/educacionfinanciera">Continúa leyendo>></a>
            </div>

        </div>
    </div>
</section>
<?php $popups = Popup::find()->all(); ?>
        <?php foreach ($popups as $popup): ?>
        <?php $popup=$popup->description ?>
        <?php $tipo=$popup->description ?>
        <?php 
            $contenido='<img src="'.$popup.'" width="98%" />'  

        ?>
        <?php endforeach ?>


<?php 
    if(@$info){
?>
    <div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content" style="height: 18%;    overflow-y: auto;">
   
    <span class="close">x</span>
    <div id="contentmodal" style="margin-top:30px;">
        <?=@$infocontent ?>
    </div>
  </div>

</div>
<?php

    }else{
         if(@$popup){
?>
    <div id="myModal" class="modal" style="    overflow-y: hidden;    z-index: 2000;">

  <!-- Modal content -->
  <div class="modal-content" style="    z-index: 2000; overflow-y: auto;     width: 55%;     padding: 2px;    margin: 8% auto;">
   
    <span class="close">x</span>
    <div id="contentmodal" style="margin-top:10px;">
        <?php
           
                echo $contenido;
            
        ?>
        
    </div>
  </div>

</div>
<?php
        }
    }
?>

<style type="text/css">

.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 12% auto; /* 15% from the top and centered */
    padding: 20px;
    border: 1px solid #888;
    width: 26%; /* Could be more or less, depending on screen size */
}

/* The Close Button */
.close {
    color: white;
    float: right;
    font-size: 11px;
    font-weight: bold;
    background: black;
    padding-left: 5px;
    padding-right: 5px;
    opacity: 1;
    height: 15px;
    margin-left: 5px;
}

.close:hover,
.close:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}
</style>
<?php 

if (@$info || @$popup)
{
	?>
	<script>
		var modal = document.getElementById('myModal');
		var btn = document.getElementById("myBtn");

		// Get the <span> element that closes the modal
		var span = document.getElementsByClassName("close")[0];

	    var datos= false;
		modal.style.display = "block";

		span.onclick = function() {
	    modal.style.display = "none";
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
	    if (event.target == modal) {
	        modal.style.display = "none";
	    }
	}
	</script>
	<?php
}

?>