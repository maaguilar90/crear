<?php
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = $title;

?>
<div class="site">
    <input type="hidden" value="<?= Yii::$app->request->csrfToken ?>" id="token" name="token">
    <div class="header text-center">
        <a class="btn btn-lg btn-success" href="<?= URL::base() ?>/site/<?= $link ?>">
            <i class="fa fa-refresh" aria-hidden="true"></i> &nbsp;Recargar</a>
    </div>
    <hr>
    <div class="body-content">
        <div style="margin: 0 auto;max-width: 500px;">
            <ul class="nav nav-tabs">
                <li class="active" id="page1"><a href="#" onclick="menu(1)">Por Código</a></li>
                <li class="" id="page2" style="display: none;"><a href="#" onclick="menu(2)">Por Cédula</a></li>
            </ul>
        </div>
        <div class="container-fluid" id="page_gye">
            <br>
            <div class="row">
                <div class="table-responsive" style="margin: 0 auto;max-width: 500px;">
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <td>
                                <form id="form_cupon">
                                    <div class="form-group">
                                        <label for="codigo">Código/cupón:</label>
                                        <input type="number" class="form-control" name="codigo" id="codigo">
                                    </div>
                                    <button type="button" class="btn btn-default" id="btn_codigo">Buscar Código</button>
                                </form>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <hr>
                <div style="text-align: center;"><h4>Resultados Base Integración</h4></div>
                <div class="table-responsive" style="margin: 0 auto;">
                    <table class="table table-striped" id="res_integra">
                    </table>
                </div>
                <hr>
                <div style="text-align: center;"><h4>Resultados Base Landing</h4></div>
                <div class="table-responsive" style="margin: 0 auto;">
                    <table class="table table-striped" id="res_landing">
                    </table>
                </div>
            </div>
            <hr>
        </div>
        <div class="container-fluid" id="page_premios" style="display: none;">
            <br>
            <div class="row">
                <div class="table-responsive" style="margin: 0 auto;max-width: 500px;">
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <td>
                                <form id="form_cedula">
                                    <div class="form-group">
                                        <label for="cedula">Cédula:</label>
                                        <input type="number" class="form-control" id="cedula" name="cedula">
                                    </div>
                                    <button type="button" class="btn btn-default" id="btn_cedula">Buscar Cédula</button>
                                </form>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <hr>
                <div style="text-align: center;"><h4>Resultados Base - Registro</h4></div>
                <div class="table-responsive" style="margin: 0 auto;">
                    <table class="table table-striped" id="res_registro">
                    </table>
                </div>
                <hr>
                <div style="text-align: center;"><h4>Resultados Base - Tickets</h4></div>
                <div class="table-responsive" style="margin: 0 auto;">
                    <table class="table table-striped" id="res_ticket">
                    </table>
                </div>
                <hr>
                <div style="text-align: center;"><h4>Resultados Base - Participación</h4></div>
                <div class="table-responsive" style="margin: 0 auto;">
                    <table class="table table-striped" id="res_participa">
                    </table>
                </div>
            </div>
            <hr>
        </div>
    </div>
</div>
