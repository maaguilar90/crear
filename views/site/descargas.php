<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'ASOPREOL | DESCARGAS';
use yii\web\View;
use app\models\Slider;
use app\models\Popup;
$script=<<< JS
var global_brand='';
$(document).ready(function() {
	
});
JS;
$aux='';
$this->registerJs($script,View::POS_END);
?>
<?php
@$info=$_GET['info'];
if (@$info==1){ $infocontent='Gracias por registrarte en el sistema. Se ha enviado un correo electrónico para que puedas acceder a nuestros servicios. '; }
if (@$info==2){ $infocontent='El usuario ingresado ya se encuentra registrado en el sistema.'; }
if (@$info==3){ $infocontent='La contraseña se ha actualizado con éxito. Ahora puede iniciar sesión y acceder a nuestros servicios.'; }
if (@$info==4){ $infocontent='El token generado es inválido. En caso de tener inconvenientes contáctese con nosotros.'; }
?>


 
        <!-- -->


<section class="container-fluid">
    <div class="servicio" style="padding: 0.5%;">
        <div style="text-align: center;">
            <!--<span style="font-size: 20px;">CESANTÍA</span>
            <div>
                <span class="line-center" style="font-size: 12px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            </div>-->
        </div>
    </div>
</section>
 
<section class="container-fluid">
    <div class="educacionfinanciera">
        <div class="column1" style="text-align: center;">
            <img style="width: 60%" src="<?= URL::base() ?>/images/site/cesantia.fw.png" >
        </div>
        <div class="column2">
            <div style=""><span class="titulo-plan"></span></div>
            <div style="font-family: 'Arial';text-align: justify; font-size: 12px; color: #595959;">
                <br>
                <span class="titulo-ef">DESCARGAS</span>
                <br>
                <span class="line-center-blue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <br>
                <span class="text-content">Glosario de Términos
                <br>
                <div class="descargas">
                    <a target="_blank" href="<?= URL::base() ?>/pdf/AsopreolGlosariodeTerminos.pdf"><img style="width: 36px;" src="<?= URL::base() ?>/images/site/pdf_icon.svg" >
                    <span class=""> Glosario de Términos</span></a>
                </div>
                <br>
                <span class="text-content">Contrato de Adhesión ASOPREOL
                <br>
                <div class="descargas">
                    <a target="_blank" href="<?= URL::base() ?>/pdf/AsopreolContratodeAdhesion.pdf"><img style="width: 36px;" src="<?= URL::base() ?>/images/site/pdf_icon.svg" >
                    <span class=""> Contrato de Adhesión ASOPREOL</span></a>
                </div>
                <br>
                <span class="text-content">Solicitud de Crédito ASOPREOL
                <br>
                <div class="descargas">
                    <a target="_blank" href="<?= URL::base() ?>/pdf/AsopreolSolicituddecredito.pdf"><img style="width: 36px;" src="<?= URL::base() ?>/images/site/pdf_icon.svg" >
                    <span class=""> Solicitud de Crédito ASOPREOL</span></a>
                </div>
                <br>
                <span class="text-content">Proyecto estatuto ASOPREOL
                <br>
                <div class="descargas">
                    <a target="_blank" href="<?= URL::base() ?>/pdf/PROYECTODEESTATUTOASOPREOLPARTICIPES.pdf"><img style="width: 36px;" src="<?= URL::base() ?>/images/site/pdf_icon.svg" >
                    <span class=""> Proyecto estatuto ASOPREOL</span></a>
                </div>
                <br>
                <span class="text-content">Proyecto Reglamento Pago de Viáticos
                <br>
                <div class="descargas">
                    <a target="_blank" href="<?= URL::base() ?>/pdf/REGLAMENTOPAGOVITICOSASOPREOLPARTICIPES.pdf"><img style="width: 36px;" src="<?= URL::base() ?>/images/site/pdf_icon.svg" >
                    <span class=""> Proyecto Reglamento Pago de Viáticos</span></a>
                </div>
                <br>
                <span class="text-content">Resolución 280-2016F
                <br>
                <div class="descargas">
                    <a target="_blank" href="<?= URL::base() ?>/pdf/Resolucion280-2016F.pdf"><img style="width: 36px;" src="<?= URL::base() ?>/images/site/pdf_icon.svg" >
                    <span class=""> Resolución 280-2016F</span></a>
                </div>
                <br>
                <span class="text-content">Plan de Educación Financiera
                <br>
                <div class="descargas">
                    <a target="_blank" href="<?= URL::base() ?>/pdf/PRESENTACIONEDUCACIONFINANCIERAASOPREOL.pdf"><img style="width: 36px;" src="<?= URL::base() ?>/images/site/pdf_icon.svg" >
                    <span class=""> Plan de Educación Financiera</span></a>
                </div>
                <br><br>
                 
            </div>

        </div>
    </div>
</section>

<section class="container-fluid">
    <div class="servicio" style="padding: 0.5%;">
        <div style="text-align: center;">
            <!--<span style="font-size: 20px;">CESANTÍA</span>
            <div>
                <span class="line-center" style="font-size: 12px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            </div>-->
        </div>
    </div>
</section>

<style type="text/css">
.text-content table
{
        width: 100%;
}
.text-content table thead
{
    padding: 2px;
}
.text-content table thead tr th
{
    padding: 2%;
    color: white;
    background-color: #1A185C;
    text-align: center;
    font-size: 14px;
    font-family: 'federo';
    border: 1px solid #1A185C;
    vertical-align: middle;
}
.text-content table tbody tr td
{
    padding: 2%;
    color: black;
    text-align: center;
    font-size: 13px;
    border: 1px solid #1A185C;
    vertical-align: middle;
}
.column1
{
  vertical-align: middle;
  width: 40% !important;
}
.column2
{
  vertical-align: middle !important;
}
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 12% auto; /* 15% from the top and centered */
    padding: 20px;
    border: 1px solid #888;
    width: 26%; /* Could be more or less, depending on screen size */
}

/* The Close Button */
.close {
    color: white;
    float: right;
    font-size: 11px;
    font-weight: bold;
    background: black;
    padding-left: 5px;
    padding-right: 5px;
    opacity: 1;
    height: 15px;
    margin-left: 5px;
}

.close:hover,
.close:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}
</style>
<?php 

if (@$info || @$popup)
{
	?>
	<script>
		var modal = document.getElementById('myModal');
		var btn = document.getElementById("myBtn");

		// Get the <span> element that closes the modal
		var span = document.getElementsByClassName("close")[0];

	    var datos= false;
		modal.style.display = "block";

		span.onclick = function() {
	    modal.style.display = "none";
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
	    if (event.target == modal) {
	        modal.style.display = "none";
	    }
	}
	</script>
	<?php
}

?>