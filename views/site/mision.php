<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'Misión | Unidad Educativa Crear';
use yii\web\View;
use app\models\Slider;
use app\models\Popup;
$script=<<< JS
var global_brand='';
$(document).ready(function() {
	
});
JS;
$aux='';
$this->registerJs($script,View::POS_END);
?>
<?php
@$info=$_GET['info'];
if (@$info==1){ $infocontent='Gracias por registrarte en el sistema. Se ha enviado un correo electrónico para que puedas acceder a nuestros servicios. '; }
if (@$info==2){ $infocontent='El usuario ingresado ya se encuentra registrado en el sistema.'; }
if (@$info==3){ $infocontent='La contraseña se ha actualizado con éxito. Ahora puede iniciar sesión y acceder a nuestros servicios.'; }
if (@$info==4){ $infocontent='El token generado es inválido. En caso de tener inconvenientes contáctese con nosotros.'; }
?>


 
        <!-- -->


 
 
<section class="container-fluid">
    <div class="educacionfinanciera">
        <div class="column1" style="text-align: center;">
            <img style="width: 60%" src="<?= URL::base() ?>/images/site/mision.fw.png" >
        </div>
        <div class="column2">
            <div style=""><span class="titulo-plan">NUESTRA</span></div>
            <div style="font-family: 'Arial';text-align: justify; font-size: 12px; color: #595959;">
                <br>
                <span class="titulo-ef">MISIÓN</span>
                <br>
                
                <span class="line-center-blue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <br>
                <span class="text-content" style="font-size: 14px;    font-style: italic; ">Para el año 2023, la Unidad Educativa CREAR estará posicionada a nivel nacional como una institución innovadora que potencia las habilidades de los estudiantes, a través del fortalecimiento del bilingüismo, de la tecnología y a la exitosa implementación programas internacionales lo que les permite incorporarse a la sociedad con propuestas creativas.</span>
                <br>
                <br>
                <br>
                <br>
                <br>&nbsp;<br>
                <br>
            </div>

        </div>
    </div>
</section>

<section class="container-fluid">
    <div class="servicio" style="padding: 0.5%;">
        <div style="text-align: center;">
            <!--<span style="font-size: 20px;">CESANTÍA</span>
            <div>
                <span class="line-center" style="font-size: 12px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            </div>-->
        </div>
    </div>
</section>

<style type="text/css">
.column1
{
  vertical-align: middle;
  width: 40% !important;
}
.column2
{
  vertical-align: middle !important;
}
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 12% auto; /* 15% from the top and centered */
    padding: 20px;
    border: 1px solid #888;
    width: 26%; /* Could be more or less, depending on screen size */
}

/* The Close Button */
.close {
    color: white;
    float: right;
    font-size: 11px;
    font-weight: bold;
    background: black;
    padding-left: 5px;
    padding-right: 5px;
    opacity: 1;
    height: 15px;
    margin-left: 5px;
}

.close:hover,
.close:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}
</style>
<?php 

if (@$info || @$popup)
{
	?>
	<script>
		var modal = document.getElementById('myModal');
		var btn = document.getElementById("myBtn");

		// Get the <span> element that closes the modal
		var span = document.getElementsByClassName("close")[0];

	    var datos= false;
		modal.style.display = "block";

		span.onclick = function() {
	    modal.style.display = "none";
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
	    if (event.target == modal) {
	        modal.style.display = "none";
	    }
	}
	</script>
	<?php
}

?>