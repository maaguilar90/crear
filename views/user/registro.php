<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'ASOPREOL | REGÍSTRATE';
use yii\web\View;
use app\models\Slider;
use app\models\Popup;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$script=<<< JS
var global_brand='';





$(document).ready(function() {
    $('#user-identity').keypress(function(e) {
    if(e.which == 13) {
        //console.log('Enter');
      //  consultar_cedula($('#user-identity').val());
    }
});


});



JS;
$aux='';
$this->registerJs($script,View::POS_END);
?>
<?php
@$info=$_GET['info'];
if (@$info==1){ $infocontent='Gracias por registrarte en el sistema. Se ha enviado un correo electrónico para que puedas acceder a nuestros servicios. '; }
if (@$info==2){ $infocontent='El usuario ingresado ya se encuentra registrado en el sistema.'; }
if (@$info==3){ $infocontent='La contraseña se ha actualizado con éxito. Ahora puede iniciar sesión y acceder a nuestros servicios.'; }
if (@$info==4){ $infocontent='El token generado es inválido. En caso de tener inconvenientes contáctese con nosotros.'; }
?>


 
        <!-- -->


<section class="container-fluid">
    <div class="servicio" style="padding: 0.5%;">
        <div style="text-align: center;">
            <!--<span style="font-size: 20px;">CESANTÍA</span>
            <div>
                <span class="line-center" style="font-size: 12px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            </div>-->
        </div>
    </div>
</section>
 
<section class="container-fluid">
    <div class="educacionfinanciera">
        <div class="column1" style="text-align: center;">
            <img style="width: 60%" src="<?= URL::base() ?>/images/site/usericon.fw.png" >
        </div>
        <div class="column2">
            <div style=""><span class="titulo-plan"></span></div>
            <div style="font-family: 'Arial';text-align: justify; font-size: 12px; color: #595959;">
                <br><br>
                <span class="titulo-ef">REGÍSTRATE</span><br>
                <span class="line-center-blue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <br>
                
                
                    <?php $form = ActiveForm::begin(); ?>

    <div style="margin-top:10px;margin-bottom:10px;">
        <span>Si eres parte de Asopreol, ingresa tu cédula: </span>
    </div>

    <?= $form->field($model, 'identity')->textInput(['maxlength' => true, 'placeholder'=>'Ej. 1704236757','pattern' => '\d*']) ?>

     <div id="errorMenssage" style="margin-top:10px;margin-bottom:10px;display:none;color:red;">
        <span>Tu cédula no es parte de nuestros registros, por favor comunícate con nosotros para formar parte de Asopreol a los teléfonos: (02) 2222910.</span>
    </div>

    <button id="btnconsultar" type="submit" class="btn btn-success" onclick="javascript:consultar_cedula()">Aceptar</button>

   

    <div id="formulario" style="display:none;">

    <?= $form->field($model, 'names')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lastnames')->textInput(['maxlength' => true]) ?>

    <!-- <?= $form->field($model, 'type')->textInput(['maxlength' => true]) ?> -->

    <?= $form->field($model, 'address_home')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_home')->textInput(['maxlength' => true, 'placeholder'=>'Ej. 022247845']) ?>

    <?= $form->field($model, 'cellphone')->textInput(['maxlength' => true, 'placeholder'=>'Ej. 0998842367']) ?>

   <!--  <?= $form->field($model, 'cellphone_company')->textInput(['maxlength' => true]) ?> -->

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>


    <!-- <?= $form->field($model, 'country_origin')->textInput(['maxlength' => true]) ?> -->

    <?= $form->field($model, 'province_residence')->textInput(['maxlength' => true, ]) ?>

    <!-- 'style' => 'text-transform: uppercase' -->

    <?= $form->field($model, 'canton_residence')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'zone_residence')->textInput(['maxlength' => true]) ?>

    <!-- <?= $form->field($model, 'chief_representative')->textInput(['maxlength' => true]) ?> -->

    <!-- <?= $form->field($model, 'secondary_representative')->textInput(['maxlength' => true]) ?> -->


    <!-- <?= $form->field($model, 'office')->textInput([]) ?> -->



   <!--  <?= $form->field($model, 'link')->textInput([]) ?> -->

    <?= $form->field($model, 'city_residence')->textInput(['maxlength' => true]) ?>


    <div class="form-group">
    
        
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    </div>

    <?php ActiveForm::end(); ?>
                 
            </div>

        </div>
    </div><br>
</section>

<section class="container-fluid">
    <div class="servicio" style="padding: 0.5%;">
        <div style="text-align: center;">
            <!--<span style="font-size: 20px;">CESANTÍA</span>
            <div>
                <span class="line-center" style="font-size: 12px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            </div>-->
        </div>
    </div>
</section>

<style type="text/css">
.text-content table
{
        width: 100%;
}
.text-content table thead
{
    padding: 2px;
}
.text-content table thead tr th
{
    padding: 2%;
    color: white;
    background-color: #1A185C;
    text-align: center;
    font-size: 14px;
    font-family: 'federo';
    border: 1px solid #1A185C;
    vertical-align: middle;
}
.text-content table tbody tr td
{
    padding: 2%;
    color: black;
    text-align: center;
    font-size: 13px;
    border: 1px solid #1A185C;
    vertical-align: middle;
}
.column1
{
  vertical-align: middle;
  width: 40% !important;
}
.column2
{
  vertical-align: middle !important;
}
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 12% auto; /* 15% from the top and centered */
    padding: 20px;
    border: 1px solid #888;
    width: 26%; /* Could be more or less, depending on screen size */
}

/* The Close Button */
.close {
    color: white;
    float: right;
    font-size: 11px;
    font-weight: bold;
    background: black;
    padding-left: 5px;
    padding-right: 5px;
    opacity: 1;
    height: 15px;
    margin-left: 5px;
}

.close:hover,
.close:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}
</style>
<?php 

if (@$info || @$popup)
{
    ?>
    <script>
        var modal = document.getElementById('myModal');
        var btn = document.getElementById("myBtn");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        var datos= false;
        modal.style.display = "block";

        span.onclick = function() {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    </script>
    <?php
}

?>
<script type="text/javascript">
	
function consultar_cedula()
{
	cedula=$('#user-identity').val();
    console.log('ajax');
    $.ajax({
       url: '<?php echo Yii::$app->request->baseUrl. '/site/findidentity' ?>',
       type: 'post',
       data: {
                 cedula: cedula
             },
       success: function (data) {
          console.log(data);
          if (data==1)
          {
            $('#formulario').fadeIn();
            //$('.help-block').fadeOut();
            $('#errorMenssage').fadeOut();
            $('#btnconsultar').fadeOut();
            $('#user-names').focus();


          }else{
            //$('#errorMenssage').css("display", "block");
            $('#errorMenssage').fadeIn();
            $('#formulario').fadeOut();
          }
       }
  });
}
</script>

 