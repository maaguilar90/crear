<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use app\models\Section;
AppAsset::register($this);
$sections=Section::find()->where(['status'=>'ACTIVE','section_id'=>NULL])->all();

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
<meta name="title" content="Crear | Unidad Educativa Crear.">
<meta name="description" content="Unidad Educativa Crear.">
<meta name="google" content="">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

    <div id="cont-general">
        <header class="container-fluid pos-relative">
        <div style="width: 100%; text-align: center;">
          <a href="<?= URL::base() ?>"><img src="<?= URL::base() ?>/images/site/logo_crear.svg" alt="Crear" class="img-logo-asoprep" style="width: 15%;padding-top: 10px;padding-left: 4%; text-align: center; float: none;"></a>
        </div>

            <!-- -->
            <nav class="navbar navbar-default" role="navigation" style="position: initial;">
              <!-- El logotipo y el icono que despliega el menú se agrupan
                   para mostrarlos mejor en los dispositivos móviles -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target=".navbar-ex1-collapse">
                  <span class="sr-only">Desplegar navegación</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
              </div>
             
              <!-- Agrupar los enlaces de navegación, los formularios y cualquier
                   otro elemento que se pueda ocultar al minimizar la barra -->
              <div class="collapse navbar-collapse navbar-ex1-collapse container">
                <ul class="nav navbar-nav" style="float:initial">
                    <li style=" "><a href="<?= URL::base() ?>/">INICIO</a></li>

                <?php foreach($sections as $section): ?>
                       <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= $section->title ?><b class="caret"></b></a>
                        <ul class="dropdown-menu">
                        <?php foreach($section->sections as $subsection): ?>

                            <?php
                            $link="#";
                            if($subsection->description){
                             $pos = strpos($subsection->description,"http");
                             $pos2 = strpos($subsection->description,"https");
                              //echo strpos($subsection->description,"http");
                             $target="";
                             if ($pos !== false || $pos2 !== false){
                                $link=$subsection->description; 
                                $target='target="_blank"';
                             }else{
                                $target="";
                                $link=URL::base().$subsection->description; 
                             }
                            }else{
                                foreach($subsection->contents as $contents){
                                    $link=Url::to(['site/content','id'=>$contents->id]);
                                    break;
                                }
                            }
                            
                             ?>
                             <?php if ($subsection->description=='http://www.crear.edu.ec/'){ $link='http://www.crear.edu.ec/'; } ?>
                          <li><a <?=@$target?> href="<?= $link ?>"><?= $subsection->title ?></a>
                            <ul><li></li></ul>
                          </li>
                          <li class="divider"></li>
                        <?php endforeach; ?>
                        </ul>
                      </li>
                  <?php endforeach; ?>
                  <li><a href="<?= Url::to(['site/contact']) ?>">CONTÁCTENOS</a></li> 
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">CUENTA<b class="caret"></b></a>
                    <ul class="dropdown-menu"> 
                       <?php if(Yii::$app->user->isGuest){ ?>
                          <li style=" "><a href="<?= Url::to(['user/registro']) ?>">Regístrate</a></li>
                          <li class="divider"></li>
                          <li style=" "><a href="<?= Url::to(['site/iniciarsesion']) ?>">Iniciar Sesión</a></li>
                      <?php }else{ ?>
                         <li style=" "><a href="<?= Url::to(['site/logout']) ?>">Cerrar Sesión (<?= Yii::$app->user->identity->names ?>)</a></li>
                         <li class="divider"></li>
                        <!-- <li style=" "><a href="<?= Url::to(['user/index']) ?>"><?= Yii::$app->user->identity->names ?></a></li>-->
                      <?php } ?>   

                    </ul>
                    
                               
                </ul>
              </div>
            </nav>
            <!-- -->
        </header>

        <?= $content ?>
     
        <footer class="container-fluid background-footer">
            <div class="container">
                <div class="col-md-15 col-sm-3 text-center">
                    <h1>CREAR</h1>
                    <a href="<?= URL::base() ?>/">Misión</a>
                    <a href="<?= URL::base() ?>/">Visión</a>
                    <h1>EDUCACIÓN</h1>
                    <a href="<?= URL::base() ?>/">Maternal</a>
                    <a href="<?= URL::base() ?>/">Educación Inicial</a>
                    <a href="<?= URL::base() ?>/">Ciclo Básico</a>
                    <a href="<?= URL::base() ?>/">Bachillerato</a>
                </div>
                <div class="col-md-15 col-sm-3 text-center">
                    <h1>ACTIVIDADES</h1>
                    <a href="<?= URL::base() ?>/pagina/manejando_tu_dinero.pdf">Noticias</a>
                    <a href="<?= URL::base() ?>/pagina/manejando_tu_dinero.pdf">Eventos</a>
                    <h1>ADMISIONES</h1>
                    <a href="<?= URL::base() ?>/">Periodo Lectivo 2018 - 2019</a>
                    <a href="<?= URL::base() ?>/">Requisitos</a>
                </div>
                <div class="col-md-15 col-sm-3 text-center">
                    <h1>SÍGUENOS EN:</h1>
                    <div style="text-align: left;">
                      <a href="https://www.facebook.com/ue.crear" style="float: none;">
                        <img class="redes-sociales" src="<?= URL::base() ?>/images/site/facebook.fw.png">
                      </a>&nbsp;&nbsp;
                      <a href="https://twitter.com/unidadeducati20" style="float: none;">
                        <img class="redes-sociales" src="<?= URL::base() ?>/images/site/twitter.fw.png">
                      </a>&nbsp;&nbsp;
                      <a href="https://www.instagram.com/ue.crear" style="float: none;">
                        <img class="redes-sociales" src="<?= URL::base() ?>/images/site/instagram.fw.png">
                      </a>
                    </div>
                    <h1>CONTÁCTANOS</h1>
                    <a href="" style="">Km. 2 1/2 Vía Samborodón, Urb. Tornero III, Calle Santa María #235.</a>
                    <a href="#"><span style="font-weight: bold;">Teléfono:</span> (04) 2831023 - 2835215 - 2835216</a>
 
                    
                </div>
            </div>
            
        </footer>
        <div class="container-fluid" style="background-color: white; font-family: 'roboto-light'; text-align: center;color: #1A185C; padding: 1%; font-size: 11px; font-weight: bold;">
                2018 CREAR. Todos los derechos reservados.
            </div>
    </div>



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
