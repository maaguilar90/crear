<?php

namespace app\controllers;

use Yii;
use app\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * UserController class_implements(class)s the CRUD actions for User model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */


    /**
     * Displays a single User model.
     * @param string $id
     * @return mixed
     */
    public function actionIndex()
    {

        $id=Yii::$app->user->identity->id;
        $model = $this->findModel($id);
             if ($model->load(Yii::$app->request->post()) && $model->save()) {
                Yii::$app->getSession()->setFlash('success','Felicidades tus datos han sido guardados con éxito.');
                return $this->goHome();
        } 

        return $this->render('index', [
            'model' => $model,
        ]);
    }
    public function actionRegistro()
    {
//        return $this->redirect(array('/index', ['info' => '1']));
        //header('Location: /asoprep/index.php?info=1');
        //exit(0);

        $model = New User;
        $model->status_client="ACTIVE";
        $model->type_client="CLIENT";
       // $model->province_residence="PRUEBA";
        if (Yii::$app->request->post()){
          $formulario=Yii::$app->request->post();
          $province_residence=strtoupper($formulario['User']['province_residence']);
          $canton_residence=strtoupper($formulario['User']['canton_residence']);
          $zone_residence=strtoupper($formulario['User']['zone_residence']);
          $city_residence=strtoupper($formulario['User']['city_residence']);
        }
                // if($email){
                //     Yii::$app->getSession()->setFlash('success','No te olvides de revisar en la bandeja de spam.');
                // }
                // else{
                //     Yii::$app->getSession()->setFlash('warning','Un error ha ocurrido por favor contactate con soporte técnico.');
                // }

        //var_dump($formulario['User']);
//&& $model->save()
             if ($model->load(Yii::$app->request->post())) {
                $model->province_residence=$province_residence;
                $model->city_residence=$city_residence;
                $model->canton_residence=$canton_residence;
                $model->zone_residence=$zone_residence;
                $length = 10;
                $chars = array_merge(range(0,9), range('a','z'), range('A','Z'));
                shuffle($chars);
                $password = implode(array_slice($chars, 0, $length));
                $token=Yii::$app->getSecurity()->generateRandomKey();
                //$token= Yii::$app->getSecurity()->generatePasswordHash($token);
                //$user=$model->find();
                // /$user= User::findOne($model->identity);
                //$token= User::generatePasswordResetToken();
                
                //$user=$model->find();
                //$user->generatePasswordResetToken();
                $user=$model->find();
                //echo 'dededed'.$token;
                 //$model->password_reset_token;
                //$hash = Yii::$app->getSecurity()->generatePasswordHash($password);

                 //echo $user;

                  $usuario = (new \yii\db\Query())
                         ->select(['auth_key'])
                         ->from('user')
                         ->where(['identity' => $model->identity])
                         ->all();

                //$model->password=$password;
               
                        //$cont=0;
                        //echo var_dump($rowpremios);
                        /*foreach($rowpremios as $k => $premios){
                            $cont=1;
                           $return['premios'][$k]=$rowpremios[$k]['icon'];
                        }*/
                    // echo   $user->password_reset_token();
                       // echo var_dump($usuario);
                    if ($usuario)
                    {
                         Yii::$app->getSession()->setFlash('success','El usuario ya exite.');
                         $info=1;
                        return $this->redirect(['site/index','info' => 2]);
                    }else
                    {
                          if ($model->save()) {
                           // echo @$usuario[0]['auth_key'];

                        $usuario = (new \yii\db\Query())
                         ->select(['auth_key'])
                         ->from('user')
                         ->where(['identity' => $model->identity])
                         ->all();      

                      // echo var_dump($model);
                      $name=$model->names;
                      $email=  Yii::$app->mailer->compose('reset', [
                      'names' => $name,
                      'url' => Yii::$app->urlManager->createAbsoluteUrl(['site/newuser','url'=>@$usuario[0]['auth_key']])
                      ])->setFrom('info@asopreol.com')
                      ->setTo($model->username)
                      ->setSubject($name." "."Información Asopreol")
                     ->send();
                            $info=1;
                            Yii::$app->getSession()->setFlash('success','Felicidades tus datos han sido guardados con éxito.');
                            //return $this->goHome();
                            //return $this->render('index', [
                             //   'info' => $info,
                            //]);
                            //return $this->render('/site/index',['model'=>$model,'info'=>$info]);
                           return $this->redirect(['site/index','info' => 1]);
                       }

                    }

               
       
        } 

        return $this->render('registro', [
            'model' => $model,
        ]);
    }






    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */




    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
