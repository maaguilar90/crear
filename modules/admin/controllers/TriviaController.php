<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\db\Query;

/**
 * Default controller for the `admin` module
 */
class TriviaController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
       
         //------ Tablas Landing ---------------------------
        $tablaReg = 'user_fiestas_2017';
        $tablaClickShare = 'user_fiestas_click_face_2017';
        $tablaTicket = 'user_fiestas_ticket_2017';

        //------------------------------------------------TOTALES------------------------------------------------
        //------ Total Registros -------------------------
        $query = new Query();
        $query->select('COUNT(DISTINCT identity) as total')
            ->from($tablaReg . ' as a')
            ->limit(1);
        $command = $query->createCommand();
        $modelReg = $command->queryAll();
        //------ Total Compartido ------------------------
        $query = new Query();
        $query->select('COUNT(DISTINCT a.identity) as total')
            ->from($tablaClickShare . ' as a')
            ->innerJoin($tablaReg . ' as b', 'a.identity=b.identity')
            ->limit(1);
        $command = $query->createCommand();
        $modelComp = $command->queryAll();
        //------ Total Compartido ------------------------
        $query = new Query();
        $query->select("COUNT(DISTINCT `identity`, `codigo`, `sucursal`, `caja`, `ticket` ) AS total")
            ->from($tablaTicket);
        $command = $query->createCommand();
        $modelCupon = $command->queryAll();

        //------------------------------------------------ X DIA ------------------------------------------------
        //------ Registro por Dia ------------------------
        $query = new Query();
        $query->select("DATE(`creation_date`) AS fecha, COUNT(DISTINCT `identity`) AS cant")
            ->from($tablaReg)
            ->groupBy('fecha');
        $command = $query->createCommand();
        $regXdia = $command->queryAll();
        //------ Ticket por Dia ------------------------
        $query = new Query();
        $query->select("DATE(`date_create`) AS fecha, COUNT(DISTINCT `identity`) AS cant")
            ->from($tablaTicket)
            ->groupBy('fecha');
        $command = $query->createCommand();
        $regTkdia = $command->queryAll();
        //------ Ticket por Dia ------------------------
        $query = new Query();
        $query->select("DATE(date_create) AS fecha, COUNT(DISTINCT identity) AS cant")
            ->from($tablaClickShare)
            ->groupBy('fecha');
        $command = $query->createCommand();
        $regCompDia = $command->queryAll();

        return $this->render('index', [
            'link' => 'trivia',
            'title' => 'Reporte | Tía - Trivia Octubre',
            'totalReg' => $modelReg[0]['total'],
            'totalComp' => 'No aplica en esta campaña',
            'totalCupon' => 'No aplica en esta campaña',
            'regPorDia' => $regXdia,
            'regTkDia' => $regTkdia,
            'regCompDia' => $regCompDia,
        ]);
    }

  
}

