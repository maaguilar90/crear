<?php
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = $title;
?>
<div class="site">
    <div class="header text-center">
        <a class="btn btn-lg btn-success" href="<?= URL::base() ?>/site/<?= $link ?>">
            <i class="fa fa-refresh" aria-hidden="true"></i> &nbsp;Recargar</a>
    </div>
    <hr>
    <div class="body-content">
        <div class="row">
            <div class="table-responsive" style="margin: 0 auto;max-width: 500px;">
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <th>Total Registrados:</th>
                        <td><?= $totalReg; ?></td>
                    </tr>
                    <tr>
                        <th>Total Compartidos:</th>
                        <td><?= $totalComp; ?></td>
                    </tr>
                    <tr>
                        <th>Total Tickets:</th>
                        <td><?= $totalCupon ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <?php if ($regPorDia): ?>
            <hr>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <div style="text-align: center;"><h4><strong>Detalle registrados por día</strong></h4></div>
                    <div class="table-responsive" style="margin: 0 auto;">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Registros por día</th>
                                <th>Total Registros</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $total = 0;
                            foreach ($regPorDia as $registro) {
                                $total += $registro['cant']; ?>
                                <tr>
                                    <td><?= $registro['fecha'] ?></td>
                                    <td><?= $registro['cant'] ?></td>
                                    <td><?= $total ?></td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-lg-3"></div>
            </div>
        <?php endif; ?>
        <?php if ($regTkDia): ?>
            <hr>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <div style="text-align: center;"><h4><strong>Detalle Tickets por día</strong></h4></div>
                    <div class="table-responsive" style="margin: 0 auto;">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Registros por día</th>
                                <th>Total Registros</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $total = 0;
                            foreach ($regTkDia as $registro) {
                                $total += $registro['cant']; ?>
                                <tr>
                                    <td><?= $registro['fecha'] ?></td>
                                    <td><?= $registro['cant'] ?></td>
                                    <td><?= $total ?></td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-lg-3"></div>
            </div>
        <?php endif; ?>
        <?php if ($regCompDia): ?>
            <hr>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <div style="text-align: center;"><h4><strong>Detalle Compartidos por día</strong></h4></div>
                    <div class="table-responsive" style="margin: 0 auto;">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Registros por día</th>
                                <th>Total Registros</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $total = 0;
                            foreach ($regCompDia as $registro) {
                                $total += $registro['cant']; ?>
                                <tr>
                                    <td><?= $registro['fecha'] ?></td>
                                    <td><?= $registro['cant'] ?></td>
                                    <td><?= $total ?></td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-lg-3"></div>
            </div>
        <?php endif; ?>
    </div>
</div>
